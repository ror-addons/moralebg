MoraleBG = {}

function MoraleBG.Init()
	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "MoraleBG.Remove")
	RegisterEventHandler(SystemData.Events.LOADING_END, "MoraleBG.Remove")
end

function MoraleBG.Remove()
    local WndName = "EA_MoraleBar"
    local mods = ModulesGetData();
    for k,v in ipairs(mods) do
	    if v.name == "VerticalMorale" then
		    if v.isEnabled then
			    WndName = WndName.."Vertical"
			else
				WndName = WndName.."Contents"
		    end
		    break
	    end
	    if v.name == "VerticalMorale-Revisted" then
		    if v.isEnabled then
			    WndName = WndName.."Vertical"
			else
				WndName = WndName.."Contents"
		    end
		    break
	    end
	end
    if (WndName == "EA_MoraleBar") then
		WndName = WndName.."Contents"
	end

	WindowSetShowing(WndName.."Background", false)
	WindowSetShowing(WndName.."Overlay", false)

	if (WndName == "EA_MoraleBarContents") then
		WindowClearAnchors(WndName.."Button3")
		WindowAddAnchor(WndName.."Button3", "bottomleft", WndName, "bottomleft", 181, -10 )

	    WindowClearAnchors(WndName.."Button4")
	    WindowAddAnchor(WndName.."Button4", "bottomleft", WndName, "bottomleft", 249, -10 )
    end
	
	--MoraleSystem.ShowSlotsForEditing( true )
end