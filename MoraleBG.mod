<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="MoraleBG" version="1.11" date="2/24/2010" >

		<Author name="Parmon/Gridgendal" email="" />
		<Description text="Removes the morale bar background" />
    <VersionSettings gameVersion="1.3.4" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Dependencies>
      <Dependency name="EA_MoraleWindow" optional="false" forceEnable="true" />
		</Dependencies>

		<Files>
			<File name="MoraleBG.lua" />
		</Files>

		<OnInitialize>
			<CallFunction name="MoraleBG.Init" />
		</OnInitialize>

		<OnUpdate/>

		<OnShutdown/>

	</UiMod>
</ModuleFile>

	